﻿using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.Net.Mime;
using System.Net.Http;
using WebApplication1.DTOs.OpenMeteo;
using WebApplication1.DTOs.VisualCrossing;
using WebApplication1.DTOs.StormGlass;

namespace WebApplication1.Utils
{
    public class HTTPRequestHelper
	{
		private static string baseUrlOpenMeteo = "https://api.open-meteo.com/v1/forecast";
		private static string baseUrlVisualCrossing = "https://weather.visualcrossing.com/VisualCrossingWebServices/rest/services/timeline/";
		private static string baseUrlStormGlass = "https://api.stormglass.io/v2/weather/point";
		private static HttpClient httpClient = new HttpClient();

		public static async Task<VisualCrossingResponse?> GetVisualCrossing(float lat, float lon)
		{
			string path =lat + "," + lon + "?unitGroup=metric" + "&key=L86L7VCQREH7XRUGV4TPRYPVF";

			HttpResponseMessage responseMessage = await httpClient.GetAsync(baseUrlVisualCrossing + path);
			if (!responseMessage.IsSuccessStatusCode) return null;
	

			HttpContent content = responseMessage.Content;
			string message = await content.ReadAsStringAsync();
			VisualCrossingResponse? visualCrossingResponse = JsonConvert.DeserializeObject<VisualCrossingResponse>(message);

			return visualCrossingResponse;
		}

		public static async Task<OpenMeteoResponse?> GetOpenMeteo(float lat, float lon)
		{
			string path = "?latitude=" + lat + "&longitude=" + lon + "&hourly=temperature_2m,precipitation_probability,precipitation,cloud_cover,visibility,wind_speed_10m";

			HttpResponseMessage responseMessage = await httpClient.GetAsync(baseUrlOpenMeteo + path);
			if (!responseMessage.IsSuccessStatusCode) return null;


			HttpContent content = responseMessage.Content;
			string message = await content.ReadAsStringAsync();

			OpenMeteoResponse? openMeteoResponse = JsonConvert.DeserializeObject<OpenMeteoResponse>(message);

			return openMeteoResponse;
		}

		public static async Task<StormGlassResponse?> GetStormGlass(float lat, float lon)
		{
			string path = "?lat=" + lat + "&lng=" + lon + "&params=cloudCover,airTemperature,currentSpeed,precipitation,visibility,windSpeed";

			httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("f7dd4c70-b6e4-11ee-8309-0242ac130002-f7dd4d24-b6e4-11ee-8309-0242ac130002");
			HttpResponseMessage responseMessage = await httpClient.GetAsync(baseUrlStormGlass + path);
			if (!responseMessage.IsSuccessStatusCode) return null;

			HttpContent content = responseMessage.Content;
			string message = await content.ReadAsStringAsync();
			httpClient.DefaultRequestHeaders.Authorization = null;
			StormGlassResponse? stormGlassResponse = JsonConvert.DeserializeObject<StormGlassResponse>(message);
			return stormGlassResponse;
		}
	}
}
