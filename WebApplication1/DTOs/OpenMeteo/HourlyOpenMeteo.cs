﻿using Newtonsoft.Json;

namespace WebApplication1.DTOs.OpenMeteo
{
    public class HourlyOpenMeteo
    {
        [JsonProperty("time")]
		public string[] Time { get; set; }
        [JsonProperty("temperature_2m")]
		public float[] Temperature { get; set; }
        [JsonProperty("precipitation_probability")]
        public float[] PrecipitationProbability { get; set; }
        [JsonProperty("precipitation")]
        public float[] Precipitation { get; set; }
        [JsonProperty("cloud_cover")]
        public int[] CloudCover { get; set; }
        [JsonProperty("visibility")]
		public float[] Visibility { get; set; }
        [JsonProperty("wind_speed_10m")]
        public float[] WindSpeed { get; set; }


    }
}
