﻿using Newtonsoft.Json;

namespace WebApplication1.DTOs.OpenMeteo
{
    public class OpenMeteoResponse
    {
        [JsonProperty("latitude")]
        private float latitude { get; set; }
        [JsonProperty("longitude")]
        private float longitude { get; set; }
        [JsonProperty("timezone")]
        private string? timezone { get; set; }
        [JsonProperty("hourly")]
        public HourlyOpenMeteo hourly { get; set; }
    }
}
