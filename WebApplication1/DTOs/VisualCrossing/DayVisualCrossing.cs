﻿using Newtonsoft.Json;

namespace WebApplication1.DTOs.VisualCrossing
{
    public class DayVisualCrossing
    {
        [JsonProperty("hours")]
        public HourVisualCrossing[] hours { get; set; }
        [JsonProperty("moonphase")]
        public float MoonPhase { get; set; }
    }
}
