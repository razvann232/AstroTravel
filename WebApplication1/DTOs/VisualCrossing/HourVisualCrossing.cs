﻿using Newtonsoft.Json;

namespace WebApplication1.DTOs.VisualCrossing
{
    public class HourVisualCrossing
    {
        [JsonProperty("datetime")]
		public string Hour { get; set; }
        [JsonProperty("temp")]
		public float Temp { get; set; }
        [JsonProperty("precipprob")]
		public float PrecipProbability { get; set; }
        [JsonProperty("visibility")]
		public float Visibility { get; set; }
        [JsonProperty("cloudcover")]
        public float CloudCover { get; set; }
        [JsonProperty("conditions")]
		public string Description { get; set; }
        [JsonProperty("windspeed")]
        public float WindSpeed { get; set; }

    }
}
