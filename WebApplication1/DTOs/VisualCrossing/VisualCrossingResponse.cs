﻿using Newtonsoft.Json;

namespace WebApplication1.DTOs.VisualCrossing
{
    public class VisualCrossingResponse
    {
        [JsonProperty("timezone")]
        private string timezone { get; set; }
        [JsonProperty("description")]
        private string description { get; set; }
        [JsonProperty("days")]
        public DayVisualCrossing[] days { get; set; }
    }
}
