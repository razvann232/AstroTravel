﻿
using Newtonsoft.Json;
using System.Text.Json.Serialization;

namespace WebApplication1.DTOs.StormGlass
{
	public class FieldStormGlass
	{
		[JsonProperty("sg")]
		public float Value {  get; set; }
	}
}
