﻿using Newtonsoft.Json;

namespace WebApplication1.DTOs.StormGlass
{
    public class StormGlassResponse
    {
        [JsonProperty("hours")]
        public HourStormGlass[] hours { get; set; }
    }
}
