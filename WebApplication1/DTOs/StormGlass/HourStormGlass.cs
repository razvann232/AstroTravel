﻿using Newtonsoft.Json;

namespace WebApplication1.DTOs.StormGlass
{
    public class HourStormGlass
    {
        [JsonProperty("airTemperature")]
        public FieldStormGlass AirTemperature { get; set; }
        [JsonProperty("cloudCover")]
        public FieldStormGlass CloudCover { get; set; }
		[JsonProperty("windSpeed")]
		public FieldStormGlass WindSpeed { get; set; }
		[JsonProperty("precipitation")]
		public FieldStormGlass Precipitation { get; set; }
		[JsonProperty("visibility")]
		public FieldStormGlass Visibility { get; set; }
		[JsonProperty("time")]
		public DateTime time { get; set; }

	}
}
