using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using WebApplication1.Models;

namespace WebApplication1.Pages
{
    [Authorize]
    public class CreateTripModel : PageModel
    {
        [BindProperty]
        public TripEssentials TripEssentials { get; set; }
        public void OnGet()
        {
        }

        public async Task<IActionResult> OnPostAsync()
        {

            return RedirectToPage("./TripDetails", TripEssentials);
        }
    }
}
