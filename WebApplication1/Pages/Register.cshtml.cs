using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using WebApplication1.Data;
using WebApplication1.Models;

namespace WebApplication1.Pages
{
    public class RegisterModel : PageModel
    {
        [BindProperty]
        public CredentialsRegister CredentialsRegister { get; set; }

        private readonly AstroTravelContext _context;
        public bool userExists = false;
        public bool registerSuccessful = false;

        public RegisterModel(AstroTravelContext context)
        {
            _context = context;

        }
        public void OnGet()
        {
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid) return Page(); 
            Credentials existingUser = await _context.Credentials.FindAsync(CredentialsRegister.Username);
            if (existingUser != null) {
                userExists = true;
                registerSuccessful = false;
                return Page();
            } else
            {
				userExists = false;
                registerSuccessful = true;
			}
            Credentials newUser = new Credentials(CredentialsRegister.Username, CredentialsRegister.Password);
            _context.Credentials.Add(newUser);
            await _context.SaveChangesAsync();
            return Page();
        }
    }
}
