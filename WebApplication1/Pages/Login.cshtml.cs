
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Routing;
using WebApplication1.Data;
using WebApplication1.Models;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;

namespace WebApplication1.Pages
{
    public class LoginModel : PageModel
    {
		private readonly AstroTravelContext _context;
        public bool LoginSuccessful = true;
        public LoginModel(AstroTravelContext context)
        {
            _context = context;
            
        }
        [BindProperty]
        public Credentials Credentials { get; set; }
        public async Task OnGetAsync()
        {
        }
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid) return Page();
			Credentials found = await _context.Credentials.FindAsync(Credentials.Username);
            if (found != null && found.Password.Equals(Credentials.Password))
            {
                var claims = new List<Claim> { new Claim(ClaimTypes.Name, found.Username) };
                var identity = new ClaimsIdentity(claims, "MyCookieAuth");
                ClaimsPrincipal claimsPrincipal = new ClaimsPrincipal(identity);
                await HttpContext.SignInAsync("MyCookieAuth", claimsPrincipal);

                return RedirectToPage("./Index");
            }
            LoginSuccessful = false;
            return Page();
        }

	}
}
