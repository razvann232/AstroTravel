using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using WebApplication1.Data;
using WebApplication1.DTOs.OpenMeteo;
using WebApplication1.DTOs.StormGlass;
using WebApplication1.DTOs.VisualCrossing;
using WebApplication1.Models;
using WebApplication1.Utils;

namespace WebApplication1.Pages
{
    [Authorize]
    public class TripDetailsModel : PageModel
    {
        public VisualCrossingResponse visualCrossingResponse { get; set; }
        public OpenMeteoResponse openMeteoResponse { get; set;}
        public StormGlassResponse stormGlassResponse { get; set;}
		[BindProperty(SupportsGet = true)]
        public TripEssentials TripEssentials { get; set; }
        [BindProperty]
        public string TripName { get; set; }
        public int SuccessfulCalls { get; set; } = 0;
        public int startDateDiff { get; set; } = 0;
        public int endDateDiff { get; set; } = 0;
		private readonly AstroTravelContext _context;

		public TripDetailsModel(AstroTravelContext context)
		{
			_context = context;

		}
		public async Task OnGetAsync()
        {
			visualCrossingResponse = await HTTPRequestHelper.GetVisualCrossing(TripEssentials.XCoord, TripEssentials.YCoord);
			openMeteoResponse = await HTTPRequestHelper.GetOpenMeteo(TripEssentials.XCoord, TripEssentials.YCoord);
            stormGlassResponse = await HTTPRequestHelper.GetStormGlass(TripEssentials.XCoord, TripEssentials.YCoord);
            if(visualCrossingResponse != null) SuccessfulCalls++; 
            if(openMeteoResponse != null) SuccessfulCalls++; 
            if(stormGlassResponse != null) SuccessfulCalls++;

            startDateDiff = TripEssentials.StartDate.DayNumber - DateOnly.FromDateTime(DateTime.Today).DayNumber;
            endDateDiff = TripEssentials.EndDate.DayNumber - DateOnly.FromDateTime(DateTime.Today).DayNumber;

		}

        public async Task<IActionResult> OnPostAsync()
        {
            TripEssentials.Name = TripName;
       
            _context.TripEssentials.Add(TripEssentials);
            await _context.SaveChangesAsync();
            TripEssentials.ID = null;
			return RedirectToPage("./TripDetails", TripEssentials);
		}
    }
}
