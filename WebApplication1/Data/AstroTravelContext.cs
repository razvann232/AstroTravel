﻿using Microsoft.EntityFrameworkCore;
using System.Diagnostics.Metrics;
using WebApplication1.Models;

namespace WebApplication1.Data
{
	public class AstroTravelContext : DbContext
	{
		public AstroTravelContext(DbContextOptions<AstroTravelContext> options): base(options)
		{

		}

		public DbSet<Credentials> Credentials { get; set; }
		public DbSet<TripEssentials> TripEssentials { get; set; }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Credentials>().ToTable("Credentials");
			modelBuilder.Entity<TripEssentials>().ToTable("TripEssentials");
		}
	}
}
