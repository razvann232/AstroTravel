﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplication1.Models
{
	public class TripEssentials
	{
		[Key]
		[Required]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int? ID { get; set; }
		[Required]
		public float YCoord { get; set; }
		[Required]
		public float XCoord { get; set; }
		[Required]
		public DateOnly StartDate { get; set; }
		[Required]
		public DateOnly EndDate { get; set; }
		public string Name { get; set; }

	}
}
