﻿using System.ComponentModel.DataAnnotations;

namespace WebApplication1.Models
{
	public class CredentialsRegister
	{
		[Key]
		[Required]
		public string? Username { get; set; }
		[Required]
		public string? Password { get; set; }
		[Compare("Password", ErrorMessage = "Password and Confirmation Password must match.")]
		[Required]
		public string? RepeatPassword { get; set; }
	}
}
