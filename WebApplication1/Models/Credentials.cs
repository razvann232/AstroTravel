﻿using System.ComponentModel.DataAnnotations;

namespace WebApplication1.Models
{
	public class Credentials
	{
		public Credentials() { }
		public Credentials(string? username, string? password)
		{
			Username = username;
			Password = password;
		}

		[Key]
		[Required]
		public string? Username {get; set;}
		[Required]
		public string? Password { get; set; }
	}
}
